package Logics;

import java.io.IOException;

import com.citi.mobileAutomation.utility.ResuablityFunctions;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flipkart.zjsonpatch.JsonDiff;

public class JsonComparsionAndFindingDifference extends ResuablityFunctions {
	
	public static void testJson() throws JsonProcessingException, IOException {
		ObjectMapper jackson = new ObjectMapper();
		JsonNode actiualJson = jackson.readTree(readFile("ACTUAL_JSON"));
		JsonNode expectedJson = jackson.readTree(readFile("EXPECTED_JSON"));
		JsonNode patchNode = JsonDiff.asJson(actiualJson, expectedJson);
		String diff = patchNode.toString();
		ResuablityFunctions.logGeneration(diff);
		System.out.println(diff);
		System.out.println(diff);
	}	
	
	public static void main(String[] args) throws JsonProcessingException, IOException {
		JsonComparsionAndFindingDifference.testJson();
	}
}
