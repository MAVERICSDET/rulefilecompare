package com.citi.mobileAutomation.utility;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.citi.mobileAutomation.controlers.InitMethodWindows;
import com.citi.mobileAutomation.controlers.InitMethodiOS;

public class ResuablityFunctions {
	
	public static String readFile(String fileName) throws IOException {
	    BufferedReader br = new BufferedReader(new FileReader(getFilePath(fileName)));
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append("\n");
	            line = br.readLine();
	        }
	        return sb.toString();
	    } finally {
	        br.close();
	    }
	}
	
	public static String getFilePath(String fileName) {
		fileName = InitMethodiOS.ProjectWorkingDirectory+"/src/main/resources/com/citi/mobileAutomation/data_source/"+fileName+".txt";
		return fileName;
		
	}
	
	public static void logGeneration(String value) {
		BufferedWriter writer = null;
		StringBuffer buffer = null;
		try {
			buffer = new StringBuffer();
			DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
			String destFile = InitMethodWindows.ProjectWorkingDirectory + "/src/main/resources/com/citi/mobileAutomation/output/"
					+"JSON_DIFFERENCE"+ dateFormat.format(new Date()) + ".txt";
			File logFile = new File(destFile);
			System.out.println(logFile.getCanonicalPath());
			writer = new BufferedWriter(new FileWriter(logFile));
			writer.newLine();
			writer.write("*************************************JSON DIFFERENCE**********************************");
			writer.newLine();
			writer.newLine();
			writer.write(value);
			writer.newLine();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (Exception e) {
			}
		}
	}

}
