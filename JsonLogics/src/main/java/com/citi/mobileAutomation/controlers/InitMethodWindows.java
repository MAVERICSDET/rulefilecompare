package com.citi.mobileAutomation.controlers;

import java.io.File;
import org.testng.ISuite;
import org.testng.ISuiteResult;

public class InitMethodWindows {	
	
	/**
	 * @Author Shenilton
	 * @Date sept 21 2018
	 */
	
	
	public static String FS = File.separator;
	public static String OSName = System.getProperty("os.name");
	public static String OSArchitecture = System.getProperty("os.arch");
	public static String OSVersion = System.getProperty("os.version");
	public static String OSBit = System.getProperty("sun.arch.data.model");
	public static String ProjectWorkingDirectory = System.getProperty("user.dir");
	public static String TestData = "/src/main/resources/com/citi/mobileAutomation/data_source/DataSource.xls";
	public static String LoginDetails = "/src/test/resources/com/citi/mobileAutomation/data_source/DataSource.xlsx";
	public static String PROPERTIES_FILE = "/src/test/resources/com/citi/mobileAutomation/config/system_variables.properties";
	public static String REPORTS = "/src/test/resources/Reports/";
	public static String IMAGES = "/src/test/resources/Reports/Images/";
	public static String VIDEOS = "/src/test/resources/Reports/Videos/";
	public static String OUTPUT_FOLDER = "/src/test/resources/Reports/";
	public static String FILE_NAME = "Extent Report.html";
	public static String SIGN_ON_RESPONSE = "/src/main/resources/com/citi/mobileAutomation/data_source/SIGN_ON_RES.txt";
	public static String RULE_FILE_RESPONSE = "/src/main/resources/com/citi/mobileAutomation/data_source/RULE_FILE.txt";
	public static String INPUT_SHEET = "/src/main/resources/com/citi/mobileAutomation/data_source/DataSource.xlsx";
	//RULE_FILE.txt
	public static ISuite suite;
	public static ISuiteResult res;
	public static String extendXML = "/extend-config.xml";
}